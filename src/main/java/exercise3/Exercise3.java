package exercise3;

public class Exercise3 {

	public static void main(String[] args) {

		FileManager fl = new FileManager("mt.txt");
		fl.parse();

		Cinta cinta = new Cinta(fl.getSimboloBlanco(), fl.getAlfabetoCinta(), "01");
		Maquina maquina = new Maquina(cinta, fl.getFuncionesTransicion(), fl.getEstadoInicial(), fl.getEstadoFinales(),
				fl.getAlfabetoMaquina());

		if (maquina.correr()) {
			System.out.println("Se acepta el string, se detecta estado final");
		} else {
			System.out.println("No se acepta el string, la maquina se detuvo sin detectar estado final");
		}
		System.out.println(cinta.getList());
	}

}
