package exercise3;

public class Transicion {

	public final ReglaArgumento argumento;
	public final ReglaAplicar aplicar;

	public Transicion(ReglaArgumento argumento, ReglaAplicar aplicar) {
		this.argumento = argumento;
		this.aplicar = aplicar;
	}

	public ReglaArgumento getArgumento() {
		return argumento;
	}

	public ReglaAplicar getAplicar() {
		return aplicar;
	}

}