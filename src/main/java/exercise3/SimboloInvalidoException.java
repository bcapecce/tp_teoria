package exercise3;

public class SimboloInvalidoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SimboloInvalidoException(String message) {
		super(message);
	}

}
