package exercise3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/*Leer la definicion de la maquina <Q,E,Rho,q0,delta,F,B>
asumo que el txt tiene un orden, y separa en cada linea
 los elementos de la definicion (son 7, de la 7-upla )*
y que las transiciones estan al final, (septima linea en adelante) */
public class FileManager {
	private String fileName;
	private List<String> alfabetoMaquina;
	private List<String> funcionesTransicion;
	private List<String> estadoFinales;
	private String simboloBlanco;
	private String estadoInicial;
	private List<String> alfabetoCinta;
	private List<String> listaEstados;
	private HashMap<ReglaArgumento, ReglaAplicar> transic = new HashMap<>();

	public FileManager(String fileName) {
		this.fileName = fileName;
	}

	public void parse() {

		ClassLoader classLoader = getClass().getClassLoader();
		String resourceFileName = classLoader.getResource(fileName).getFile();

		BufferedReader br = null;
		String strCurrentLine;
		List<String> lineas = new ArrayList<String>();

		try {
			br = new BufferedReader(new FileReader(resourceFileName));
			while ((strCurrentLine = br.readLine()) != null) {
				// System.out.println (strCurrentLine);
				lineas.add(strCurrentLine);
			}

			String conjDeEstados = lineas.get(0);// 1 es Set
			loadStatesList(conjDeEstados);//

			String alfabInput = lineas.get(1);// 2 es set
			cargarAlfabetoInput(alfabInput);

			String alfaCinta = lineas.get(2);// 3 es list
			loadTapeAlphabet(alfaCinta);

			String estadFinales = lineas.get(3);// 4 es set
			cargarConjEstFinales(estadFinales);

			simboloBlanco = lineas.get(4); // 5 es String

			estadoInicial = lineas.get(5);// 6 es String
			// String funcTransicion
			// Desde esta parte del archivo necesito recorrer
			// todo lo que queda del archivo
			funcionesTransicion = new ArrayList<>();
			for (int i = 6; i < lineas.size(); i++) {
				// System.out.println(lineas.size());
				// System.out.println(lineas.get(i));
				funcionesTransicion.add(lineas.get(i));

			}
			mapearTransiciones();

		}
		// Captura de excepción por fichero no encontrado
		catch (FileNotFoundException ex) {
			System.out.println("Error: Fichero no encontrado");
			ex.printStackTrace();
		}
		// Captura de cualquier otra excepción
		catch (Exception ex) {
			System.out.println("Error de lectura del fichero");
			ex.printStackTrace();
		}
		// Asegurar el cierre del fichero en cualquier caso
		finally {
			try {
				// Cerrar el fichero si se ha podido abrir
				if (br != null) {
					br.close();
				}
			} catch (Exception ex) {
				System.out.println("Error al cerrar el fichero");
				ex.printStackTrace();
			}
		}
	}

	private void loadStatesList(String conjDeEstados) {
		String alphabetWithoutSpaces = conjDeEstados.replace(" ", "");
		listaEstados = Arrays.asList(alphabetWithoutSpaces.split(","));
	}

	private void mapearTransiciones() {

		for (String string : funcionesTransicion) {
			String alphabetWithoutSpaces = string.replace(" ", "");
			List<String> partes = Arrays.asList(alphabetWithoutSpaces.split("="));
			String argumento = partes.get(0);
			String regla = partes.get(1);

			String[] partesArgumento = argumento.split(",");
			String[] partesRegla = regla.split(",");
			String estado = partesArgumento[0];
			String simbol = partesArgumento[1];
			ReglaArgumento ra = new ReglaArgumento(estado, simbol);
			String estadoIr = partesRegla[0];
			String simboloIr = partesRegla[1];
			// movimiento
			Direccion dir = null;
			if ("R".equals(partesRegla[2])) {
				dir = Direccion.DERECHA;
			} else {
				dir = Direccion.IZQUIERDA;
			}

			ReglaAplicar aplic = new ReglaAplicar(estadoIr, simboloIr, dir);
			Transicion tran = new Transicion(ra, aplic);

			transic.put(tran.getArgumento(), tran.getAplicar());

		}

	}

	private void loadTapeAlphabet(String alfaCinta) {
		String alphabetWithoutSpaces = alfaCinta.replace(" ", "");
		alfabetoCinta = Arrays.asList(alphabetWithoutSpaces.split(","));
	}

	private void cargarAlfabetoInput(String line) {

		String alphabetWithoutSpaces = line.replace(" ", "");
		alfabetoMaquina = Arrays.asList(alphabetWithoutSpaces.split(","));
	}

	public HashMap<ReglaArgumento, ReglaAplicar> getFuncionesTransicion() {
		return transic;
	}

	public List<String> getAlfabetoCinta() {
		return alfabetoCinta;
	}

	public void setAlfabetoMaquina(List<String> alfabetoMaquina) {
		this.alfabetoMaquina = alfabetoMaquina;
	}

	private void cargarConjEstFinales(String line) {
		String alphabetWithoutSpaces = line.replace(" ", "");
		estadoFinales = Arrays.asList(alphabetWithoutSpaces.split(","));
	}

	public List<String> getAlfabetoMaquina() {
		return alfabetoMaquina;
	}

	public List<String> getEstadoFinales() {
		return estadoFinales;
	}

	public String getSimboloBlanco() {
		return simboloBlanco;
	}

	public String getEstadoInicial() {
		return estadoInicial;
	}

	public List<String> getListaEstados() {
		return listaEstados;
	}

}
