package exercise3;

public class ReglaAplicar {

	public final String estado;
	public final Direccion movimiento; // movida del cabezal despues de escribir en la cinta (un cinta.mover?)
	public final String simbolo;

	public ReglaAplicar(String estado, String simbolo, Direccion movimiento) {
		// si proximoestado es null arrojar excepcion
		this.simbolo = simbolo;
		this.movimiento = movimiento;
		this.estado = estado;
	}

	public String getEstado() {
		return estado;
	}

	public Direccion getMovimiento() {
		return movimiento;
	}

	public String getSimbolo() {
		return simbolo;
	}

}
