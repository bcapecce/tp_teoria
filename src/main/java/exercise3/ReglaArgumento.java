package exercise3;

public class ReglaArgumento {
	// dividi la transicion como una regla
	// con dos partes: el argumento y su aplicacion
	// son los obj REglaArgumento y ReglaAaplicar //repectivamente
	// estoy en q y viene x, buscar la regla correcta
	// a aplicar
	public final String estado;
	public final String simbolo;

	public ReglaArgumento(String estado, String simbolo) {
		this.estado = estado;
		this.simbolo = simbolo;
	}

	public String getEstado() {
		return estado;
	}

	public String getSimbolo() {
		return simbolo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((simbolo == null) ? 0 : simbolo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReglaArgumento other = (ReglaArgumento) obj;
		if (estado == null) {
			if (other.estado != null)
				return false;
		} else if (!estado.equals(other.estado))
			return false;
		if (simbolo == null) {
			if (other.simbolo != null)
				return false;
		} else if (!simbolo.equals(other.simbolo))
			return false;
		return true;
	}

}
