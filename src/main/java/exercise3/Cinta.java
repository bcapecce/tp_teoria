package exercise3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Cinta {

	private int posicionActual = 0;

	private LinkedList<String> list = new LinkedList<>();
	private Set<String> alfabeto;
	private String simboloBlanco;

	// se tiene que expandir si se exceden los rangos de la cinta
	public Cinta(String simboloBlanco, List<String> alfabeto, String w) {
		this.posicionActual = 0;
		this.list = new LinkedList<>(Arrays.asList(w.split("")));
		this.simboloBlanco = simboloBlanco;
		this.alfabeto = new HashSet<>(alfabeto);

		for (String simbolo : list) {
			if (!alfabeto.contains(simbolo)) {
				throw new SimboloInvalidoException(
						String.format("El simbolo %s no se encuentra definido en el alfabeto", simbolo));
			}
		}
	}

	public String getSimboloActual() {
		return list.get(posicionActual);
	}

	public void pisar(String simbolo) {
		if (!simboloBlanco.equals(simbolo) && !alfabeto.contains(simbolo)) {
			throw new SimboloInvalidoException(
					String.format("El simbolo %s no se encuentra definido en el alfabeto", simbolo));
		}
		this.list.set(posicionActual, simbolo);
	}

	public void mover(Direccion tm) {

		if (tm == Direccion.DERECHA) {
			posicionActual = posicionActual + 1;
		} else {
			posicionActual = posicionActual - 1;
		}
		if (posicionActual < 0) {
			this.list.addFirst(simboloBlanco);
			posicionActual = 0;
		}
		if (posicionActual >= list.size()) {
			this.list.addLast(simboloBlanco);
		}
	}

	public LinkedList<String> getList() {
		return list;
	}

	public int getPosicionActual() {
		return posicionActual;
	}
}
