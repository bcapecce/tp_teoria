package exercise3;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Maquina {

	private Cinta cinta;
	private Map<ReglaArgumento, ReglaAplicar> funcTransic;
	private int instrucciones;
	private int maxInstrucciones;
	private String estadoActual;
	private Set<String> estadosFinales;
	private Set<String> alfabeto;

	public Maquina(Cinta cinta, Map<ReglaArgumento, ReglaAplicar> funcTransic, String estadoActual,
			List<String> estadosFinales, List<String> alfabeto) {
		this.cinta = cinta;
		this.funcTransic = funcTransic;
		this.estadoActual = estadoActual;
		this.estadosFinales = new HashSet<>(estadosFinales);
		this.alfabeto = new HashSet<>(alfabeto);
		this.instrucciones = 0;
		this.maxInstrucciones = cinta.getList().size() * this.funcTransic.size() * this.alfabeto.size();
	}

	public boolean correr() {
		while (instrucciones < maxInstrucciones) {
			instrucciones++;
			String simbolo = cinta.getSimboloActual();
			ReglaArgumento ra = new ReglaArgumento(this.estadoActual, simbolo);
			if (funcTransic.containsKey(ra)) {
				ReglaAplicar ra2 = funcTransic.get(ra);
				this.estadoActual = ra2.getEstado();
				this.cinta.pisar(ra2.getSimbolo());
				this.cinta.mover(ra2.getMovimiento());
			} else {
				// Si no encuentra funciona de transición la maquina se detiene
				return false;
			}
			if (estadosFinales.contains(estadoActual)) {
				// Si encuentra un estado final, la maquina se detiene
				return true;
			}
		}
		// si se supera la cantidad máxima de instrucciones, la maquina se detiene
		return false;
	}
}
