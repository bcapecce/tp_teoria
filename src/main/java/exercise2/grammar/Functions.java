package exercise2.grammar;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import exercise2.enums.VariableType;
import exercise2.models.Production;
import exercise2.models.ProductionVariable;
import exercise2.models.Variable;

public class Functions {
	
	private Map<String, Variable> variables = new HashMap<>();
	private List<Production> productions;
	
	public Map<String, Variable> searchFirstAndNexts(List<Variable> elements, List<Production> productions){
		this.productions = productions;

		this.findFirsts(elements);
		this.findAllNexts();
		
		return this.variables;
	}
	/**
	 * Este metodo carga todos los primeros de cada elemento e iterara hasta que 
	 * todos los elementos esten cargados con sus primeros en el map de primeros
	 * @param elements
	 */
	protected void findFirsts(List<Variable> elements){

		int index = 0;
		int countElements = elements.size();

		while(countElements > variables.size()) {
			Variable elemento = elements.get(index);
			
			if(elemento.isTerminal()) {
				
				elemento.setFirsts(new HashSet<>(Arrays.asList(new ProductionVariable(elemento,null))));
				variables.put(elemento.getValue(), elemento);
				elements.remove(index);
			}else {
				Set<ProductionVariable> firstsNew = findFirsts(elemento, elements);
				//if(!firstsNew.toString().contains(VariableType.VARIABLE.toString())) {
					addFirst(elemento,elements, firstsNew);
				//}
			}
			
		}	
	}
	
	/**
	 * Este metodo se encargar de buscar los primeros de un elemento, en el caso de
	 * que uno de los primeros sea una variable se llamara de nuevo recursivamente para obtener
	 * los primeros de la variables hasta que encuentre los primeros y no sean variables sino terminales.Cuando vaya volviendo 
	 * de la recursividad cargara los primeros de las variables que encontro
	 * @param elementToFind elemento del cual se quiere obtener los primeros
	 * @param elementsToFind lista de elementos que faltan buscar los primeros , en el caso de que se llame recursivamente y encuentre
	 * primeros de otras variables se iran eliminando de esta lista y se cargaran en el map de primeros.
	 * @return
	 */
	protected Set<ProductionVariable> findFirsts(Variable elementToFind, List<Variable> elementsToFind){
		Set<ProductionVariable> newFirsts = getFirstForVariable(elementToFind);
		Set<ProductionVariable> newFirstFinal = new HashSet<>();
		boolean containsVariable = newFirsts.stream().map(p -> p.getVariable().getType().toString()).collect(Collectors.toList()).toString().contains(VariableType.VARIABLE.toString());
		if(containsVariable) {
			for(ProductionVariable element : newFirsts) {
				if(element.getVariable().isVariable()) {
					Set<ProductionVariable> firstsVariable = findFirsts(element.getVariable(), elementsToFind);
					addFirstToList(firstsVariable, newFirstFinal, element.getProduction());
					addFirst(element.getVariable(), elementsToFind, firstsVariable);
				}else {
					newFirstFinal.add(element);
				}
			}
		}else {
			newFirstFinal = newFirsts;
		}
		return newFirstFinal;
	}
	/**
	 * Este metodo se encargar de agregar los primeros a una variable y eliminarlo de la lista de 
	 * variables que faltan encontrar los primeros
	 * @param element elemento al cual se le agregaran los primeros
	 * @param elementsToFind lista de variables a encontrar los primeros
	 * @param firsts primeros que se agregaran a la variable
	 */
	private void addFirst(Variable element, List<Variable> elementsToFind, Set<ProductionVariable> firsts) {
		element.setFirsts(firsts);
		variables.put(element.getValue(), element);
		elementsToFind.remove(element);
	}
	/**
	 * Tenemos este caso
	 * X_{1} -> X_{2}
	 * X_{2} -> [a]
	 * X_{2} -> [b]
	 * Luego tenemos que agregar los primero de X_{2} a X_{1} que serian a y b pero tenemos que 
	 * cambiar la produccion ya que los primeros de  X_{2} aparecen en esta produccion 
	 * X_{2} -> [a]
	 * X_{2} -> [b]
	 * Pero para X_{1} la produccion seria esta X_{1} -> X_{2}
	 * @param firstsVariable Primeros de la variable en el caso que hablamos seria X_{2}
	 * @param newFirstFinal los primero de X_{1}
	 * @param production este tiene la produccion donde se tienen los primeros por transitividad, en nuestro ejemplo seria
	 * X_{1} -> X_{2}
	 */
	private void addFirstToList(Set<ProductionVariable> firstsVariable, Set<ProductionVariable> newFirstsFinal, Production production ) {
		Set<ProductionVariable> otherFirsts = new HashSet<>();
		firstsVariable.stream().forEach(p -> otherFirsts.add(new ProductionVariable(p.getVariable(), production)));
		addAll(newFirstsFinal, otherFirsts);
	}
	
	/**
	 * Este metodo obtiene todos los primeros de una variable obteniendola de las producciones
	 * no es un terminal
	 * @param element variable de la cual se quiere obtener los primero
	 * @return primeros del elemento evaluado
	 */
	private Set<ProductionVariable> getFirstForVariable(Variable element){		
		return this.productions.stream()
				.filter(p -> p.getPartLeft().equals(element))
				.map(p -> new ProductionVariable(p.getPartRigth().get(0),p))
				.collect(Collectors.toSet());
	}
	
	/**
	 * Este metodo recorre toda la lista de variables y va cargando los siguientes de cada variable
	 */
	public void findAllNexts() {
		
		this.variables.entrySet().stream()
		.map(Entry::getValue)
		.filter(Variable::isVariable)
		.forEach(element -> {
				Set<ProductionVariable> setNexts = findNextsOfElement(element);
				setNexts.add(new ProductionVariable(new Variable(Variable.FINAL, VariableType.FINAL),null));
				element.setNexts(setNexts);
		});
		
	}
	
	/**
	 * Este metodo se encarga de buscar en todas las producciones los siguientes del elemento
	 * @param element
	 * @return
	 */
	protected Set<ProductionVariable> findNextsOfElement(Variable element){
		Set<ProductionVariable> nexts = new HashSet<>();
		
		//recorro las producciones
		for(Production production : productions) {
			if (production.existInPartRigth(element)) {
				//recorro los items de la parte derecha de la produccion
				Variable var = production.getPartRigth().get(production.getPartRigth().size()-1);
				if(!element.equals(production.getPartLeft()) || 
						!element.equals(var)){
					findNexts(element, production, nexts);

				}
			}
		}
		
		return nexts;
	}

	/**
	 * Este metodo recorre todos los elementos de la parte derecha de una produccion buscando el 
	 * el elemento pasado por parametro.Cuando lo encuentra evalua que tiene a los costado
	 * regla 1 
	 * @param element a -> b element{X_{2}}
	 * @param production
	 * @param nexts
	 */
	private void findNexts(Variable element, Production production, Set<ProductionVariable> nexts) {
		
		for (int i = 0; i < production.getPartRigth().size(); i++) {
			List<Variable> partRigth = production.getPartRigth();

			if (element.equals(partRigth.get(i))) {
				
				if (this.firstRule(i, partRigth.size(),partRigth)) {
					addNextElement(nexts, this.variables.get(partRigth.get(i + 1).getValue()), production);
					
				} else if ( this.secondRule(i, partRigth.size(),partRigth)) {
					addNextElement(nexts, production);
					
				}
			}
		}
	}
	/**
	 * Este elemento se encargar de agregar elementos a la lista de siguientes
	 * En el caso de que sea terminal se agrega directo el elemento, caso contrario se
	 * agregan los primeros del siguiente elemento
	 * @param setNexts
	 * @param nextElement
	 */
	private void addNextElement(Set<ProductionVariable> nexts, Variable nextElement,Production production) {
		if(nextElement.isTerminal()) {
			nexts.add(new ProductionVariable(nextElement,production));
		}else {
			Set<ProductionVariable> newNexts = nextElement.getFirsts().stream()
					.filter(p -> !p.getVariable().isNull())
					.collect(Collectors.toSet());
			addAll(nexts, newNexts);
		}
	}
	/**
	 * Este metodo se encargar de agregar los siguientes, en el caso de que los siguiente 
	 * de la parte izquieda de la produccion sea nula quiere decir que todavia no se calculo por ende
	 * se deja una marca para cargarlos mas adelante.En el caso que no sean nulos se agregan los elementos.
	 * @param setNexts
	 * @param production
	 */
	private void addNextElement(Set<ProductionVariable> nexts, Production production) {
		Variable variable = this.variables.get(production.getPartLeft().getValue());
		Set<ProductionVariable> newNexts = variable != null && variable.getNexts() != null && !variable.getNexts().isEmpty()? getNexts(variable) : null;
		if (!Objects.isNull(newNexts)) {
			addAll(nexts, newNexts);
		} else {
			addAll(nexts,findNextsOfElement(production.getPartLeft()));
			
		}
	}
	
	private Set<ProductionVariable> getNexts(Variable variable){
		return variable.getNexts().stream()
		.filter(p -> !p.getVariable().isFinal()).collect(Collectors.toSet());
	}
	
	/**
	 * Este metodo devolvera true si cumple algunas de estas condiciones
	 * 1 no hay un elemto a la derecha
	 * 2 el elemento de al lado es null ejemplo: X_{1} -> X_{2}&
	 * 3 el elemento de al lado es nulleable ejemplo: X_{1} -> X_{2}X_{3}
	 * 												  X_{3} -> &
	 * & = null
	 * @param indexElement
	 * @param production
	 * @return
	 */
	private boolean secondRule(int indexElement, int elementCount, List<Variable> variables) {
		return thereIsntAnElementToRight(indexElement,elementCount) || variables.get(indexElement+1).isNull()
				|| isNullable(variables.get(indexElement+1));
	}
	
	/**
	 * Este metodo evalua que no haya ningun elemento a la derecha
	 * @param indexElement
	 * @param elementCount
	 * @return
	 */
	private boolean thereIsntAnElementToRight(int indexElement, int elementCount) {
		return indexElement + 1  >= elementCount;
	}
	
	
	/**
	 * Este metodo devolver true si cumple todas estas condiciones
	 * 1 hay un elemento a la derecha
	 * 2 el elemento que esta a la derecha no es null
	 * 3 el elemento que esta a la derecha no es nullable
	 * ejemplo
	 * X_{1} -> X_{2}X_{3}
	 * X_{3} -> a
	 * o
	 * X_{1} -> X_{2}b
	 * @param indexElement
	 * @param production
	 * @return
	 */
	private boolean firstRule(int indexElement, int elementCount, List<Variable> variables) {
		return thereIsElementToRight(indexElement, elementCount) && !variables.get(indexElement+1).isNull() 
				&& !isNullable(variables.get(indexElement+1));
	}
	
	/**
	 * Este metodo evalua que haya un elemento a la derecha
	 * @param indexElement
	 * @param elementCount
	 * @return
	 */
	private boolean thereIsElementToRight(int indexElement, int elementCount) {
		return indexElement + 1 < elementCount;
	}
	
	private boolean isNullable(Variable variable) {
		Variable var = this.variables.get(variable.getValue());
		if(var.isVariable() && var.getFirsts() != null && !var.getFirsts().isEmpty()) {
			return var.getFirsts().stream().filter(p -> p.getVariable().isNull()).count() > 0;
		}
		return false;
	}
	
	private void addAll(Set<ProductionVariable> firstFinal, Set<ProductionVariable> newFirst) {
		List<String> values = firstFinal.stream().map(p -> p.getVariable().getValue()).collect(Collectors.toList());
		newFirst.stream().forEach(p -> {
			if(!values.contains(p.getVariable().getValue())) {
				firstFinal.add(p);
			}
		});
	}
	public List<Production> getProductions() {
		return productions;
	}
	public void setProductions(List<Production> productions) {
		this.productions = productions;
	}
	public Map<String, Variable> getVariables() {
		return variables;
	}
	public void setVariables(Map<String, Variable> variables) {
		this.variables = variables;
	}
	
}
