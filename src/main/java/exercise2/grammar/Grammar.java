
package exercise2.grammar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import exercise2.enums.VariableType;
import exercise2.exceptions.GrammarException;
import exercise2.mapper.Mapper;
import exercise2.models.Input;
import exercise2.models.Production;
import exercise2.models.ProductionVariable;
import exercise2.models.RowColumnParsingTable;
import exercise2.models.Variable;


public class Grammar {

	public static final String VARIABLE = "X_{";
	public static final String TERMINAL = "[a-z]";
	public static final String FINAL = "$";
	
	private List<Production> productions = new ArrayList<>();
	private Map<String, Variable> variables = new HashMap<>();
	private Map<RowColumnParsingTable, Production> parsingTable = new HashMap<>();
	private LinkedList<Variable> stack = new LinkedList<>();
	private Mapper mapper = new Mapper();
	private Functions functions = new Functions();
	private String inputToValidate;
	
	public void setProductions(List<String> productions) throws GrammarException {
		this.productions = mapper.mapStringsToProductions(productions);
		variables = functions.searchFirstAndNexts(mapper.getListElements(), this.productions);
		variables.put("$",new Variable("$",VariableType.FINAL));
		this.loadParsingTable();
				
	}

	public List<Production> getProductions(){
		return productions;
	}
	
	/**
	 * Este metodo se encargar de obtener todos los terminales por un lado y todos las variables por otro
	 * lado para luego iterarlos e ir construyendo la tabla de parsing
	 * @param elements
	 */
	protected void loadParsingTable() {
		List<Variable> nonTerminals = variables.entrySet().stream().map(Entry::getValue)
				.filter(Variable::isVariable).collect(Collectors.toList());
		List<Variable> inputSymbols = variables.entrySet().stream().map(Entry::getValue)
				.filter(p -> !p.isVariable()).collect(Collectors.toList());
		
		nonTerminals.stream().forEach(p -> inputSymbols.forEach(r -> loadParsingTable(p, r)));
		
	}
	/**
	 * Este metodo se encargar de agregar un elemento a la tabla de parsing
	 * En caso de que el inputSymbol sea nulo se buscan los siguientes, caso contrario se agregar los
	 * primeros que se obtienen el nonTerminal y el inputSymbol 
	 * @param p
	 * @param r
	 */
	private void loadParsingTable(Variable nonTerminal, Variable inputSymbol) {
		Optional<Production> production = findProduction(nonTerminal, inputSymbol);
		 if(inputSymbol.isNull() && production.isPresent()) {
			 loadNextInParsingTable(nonTerminal, production.get());
		 }else {
			 parsingTable.put(new RowColumnParsingTable(inputSymbol,nonTerminal), production.isPresent() ? production.get() : null);

		 }
	}
	
	private void loadNextInParsingTable(Variable variable, Production production) {
		variable.getNexts().stream().forEach(s -> {
			 RowColumnParsingTable rowColumnParsingTable = new RowColumnParsingTable(s.getVariable(),variable);
			 if(parsingTable.get(rowColumnParsingTable) == null) {
				 parsingTable.put(rowColumnParsingTable, production);
			 }
		 });
	}
	
	/**
	 * Este metodo se encarga de validar si el string ingresado pertenece a la gramatica, para eso se apoya
	 * en la tabla de parsing atributo parsingTable y la pila atributo stack
	 * @param value
	 * @return
	 */
	public boolean validateString(String value) {
		inputToValidate = value;
		Input input = new Input( value.concat("$"));
		stack.add(this.productions.get(0).getPartLeft());

		try {
			while(value.length()>0) {
				
				stack = addElementsToStack(input);
				advanceWhileThereAreTerminals(input);

				if(input.isFinal() && stack.isEmpty())
					break;
			}
			
			System.out.println("El string : "+inputToValidate+" pertenece a la gramatica");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
		
	}
	/**
	 * Este metodo se encargar de avanzar en el caso el elemento del input sea igual que el primer
	 * elemento de la pila
	 * @param stackElement
	 * @param input
	 * @throws GrammarException
	 */
	public void advanceWhileThereAreTerminals(Input input) throws GrammarException {
		boolean thereAreTerminals = true;
		
		while (thereAreTerminals) {
			Variable stackElement = getfirstElementToStack(input.getFirstElement());

			if(stackElement.isVariable()) {
				thereAreTerminals = false;
			}else {
				
				if(!stackElement.isNull()) {
					input.advance();
				}
				
				if(input.isEmptyOrFinal()) {
					thereAreTerminals = false;
				}
				stack.removeFirst();
				
			}
		}
		
	}
	/**
	 * Este metodo se encarga de obtener el primer elemento de la pila,
	 * en el caso de que no pase la validacion se arroja un error 
	 * @param inputElement
	 * @return
	 * @throws GrammarException
	 */
	private Variable getfirstElementToStack(String inputElement) throws GrammarException {
		
		if(entryDoesntBelongToTheGrammar(inputElement)) {
			throw new GrammarException("El string : "+this.inputToValidate+" no pertence a la gramatica");
		}else {
			return stack.getFirst();
		}
	}
	
	/**
	 * Este metodo se encargar de validar si el elemento del input esta en la pila
	 * Casos:
	 * *Si la pila esta vacia quiere decir que el elemento del input no se encuentra por ende el input no pretence a la gramatica
	 * *Si el primer elemento de la pila no es igual al elemento del input y es un terminal entonces 
	 * el elemento del input no se encuentra por ende el input no pretence a la gramatica   
	 * @param inputElement
	 * @return
	 */
	private boolean entryDoesntBelongToTheGrammar(String inputElement) {
		return stack.isEmpty() || (!stack.getFirst().getValue().equals(inputElement) 
					&& stack.getFirst().isTerminal());
	}

	/**
	 * Este metodo se encargar de agregar la parte derecha de la produccion, que se obtiene de la tabla de parsing, a la pila.
	 * En caso de que no haya produccion quiere decir que el input que se quiere validar no pertenece a la gramatica
	 * @param input
	 * @param stack
	 * @return
	 * @throws GrammarException
	 */
	private LinkedList<Variable> addElementsToStack(Input input) throws GrammarException {
		
		RowColumnParsingTable rowColumnParsingTable = this.buildRowColumnParsingTable(input.getFirstElement(), stack.getFirst());
		
		return Optional.ofNullable(this.parsingTable.get(rowColumnParsingTable)).map(production -> {
			stack.removeFirst();
			stack.addAll(0, production.getPartRigth());
			return stack;
		}).orElseThrow(() -> new GrammarException("El string : "+inputToValidate+" no pertence a la gramatica"));

	}
	
	private Variable buildVariable(String inputElement) {
		return inputElement.equals("$") ? new Variable(inputElement, VariableType.FINAL) 
										:new Variable(inputElement);
	}
	private RowColumnParsingTable buildRowColumnParsingTable(String inputElement, Variable stackElement) {
		return new RowColumnParsingTable(this.buildVariable(inputElement), stackElement);
	}
	
	
	
	/**
	 * Este metodo se encarga de encontrar en los primeros de nonTerminal la variable input
	 * @param nonTerminal
	 * @param input
	 * @return
	 */
	private Optional<Production> findProduction(Variable nonTerminal, Variable input) {
		return nonTerminal.getFirsts().stream()
				.filter(p -> p.getVariable().equals(input))
				.map(ProductionVariable::getProduction)
				.findAny();
	}
	
	public Map<RowColumnParsingTable, Production> getParsingTable() {
		return parsingTable;
	}
	public void setParsingTable(Map<RowColumnParsingTable, Production> parsingTable) {
		this.parsingTable = parsingTable;
	}
	
	
	public Map<String, Variable> getVariables() {
		return variables;
	}
	public void setVariables(Map<String, Variable> variables) {
		this.variables = variables;
	}
	public List<Variable> getListElements(){
		return mapper.getListElements();
	}
	
}
