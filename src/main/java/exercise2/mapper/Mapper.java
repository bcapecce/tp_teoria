package exercise2.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import exercise2.enums.VariableType;
import exercise2.models.Production;
import exercise2.models.Variable;

public class Mapper {

	private Set<Variable> elements = new HashSet<>();
	
	/**
	 * Este metodo se encarga de recibir una linea de string y parsearla a una produccion
	 * con su lado izquierdo y derecho
	 * @param line
	 * @return
	 */
	protected Production mapStringToProduction(String line) {
		
		Variable variable = new Variable(line.substring(0, line.indexOf("->")).replace(" ", ""));
		String body = line.substring(line.indexOf("->")+2,line.length()).replace(" ", "");
		List<Variable> partRigth =  getPartRight(body);
		elements.add(variable);
		elements.addAll(partRigth);
		return new Production(variable,partRigth);
	}
	
	public List<Production> mapStringsToProductions(List<String> lines){
		return lines.stream()
				.map(this :: mapStringToProduction)
				.collect(Collectors.toList());
	}
	/**
	 * Este metodo recibe un string que es la parte derecha de la produccion y la devuelve 
	 * en formato de variable
	 * @param body
	 * @return
	 */
	private List<Variable> getPartRight(String body){
		List<Variable> partRigth = new ArrayList<>();
				
		body = body.replace(" ", "");
		while(body.length() > 0) {
			
			if(isTerminalOrNull(body)) {
				String element = String.valueOf(body.charAt(0));
				partRigth.add(element.equals(Variable.NULL) ? new Variable(element, VariableType.NULL) 
												   : new Variable(element));
				body = body.substring( 1, body.length());

			}else if(body.contains(Variable.VARIABLE)){
				String variable = body.substring(body.indexOf(Variable.VARIABLE), body.indexOf("}")+1);
				partRigth.add(new Variable (variable));
				body = body.substring( body.indexOf("}")+1, body.length());
			}
		}
		
		
		return partRigth;
	}
	
	private boolean isTerminalOrNull(String body) {
		return String.valueOf(body.charAt(0)).matches(Variable.TERMINAL) || String.valueOf(body.charAt(0)).equals(Variable.NULL);
	}

	public Set<Variable> getElements() {
		return elements;
	}
	public List<Variable> getListElements() {
		return elements.stream().collect(Collectors.toList());
	}
	
}
