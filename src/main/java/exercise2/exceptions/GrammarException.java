package exercise2.exceptions;

public class GrammarException extends Exception{

	public GrammarException(String message) {
		super(message);
	}
	
}
