package exercise2.models;

public class RowColumnParsingTable {

	private Variable inputSymbol;
	private Variable nonTerminal;
	
	public RowColumnParsingTable(Variable inputSymbol, Variable nonTerminal) {
		super();
		this.inputSymbol = inputSymbol;
		this.nonTerminal = nonTerminal;
	}

	public Variable getInputSymbol() {
		return inputSymbol;
	}

	public void setInputSymbol(Variable inputSymbol) {
		this.inputSymbol = inputSymbol;
	}

	public Variable getNonTerminal() {
		return nonTerminal;
	}

	public void setNonTerminal(Variable nonTerminal) {
		this.nonTerminal = nonTerminal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inputSymbol == null) ? 0 : inputSymbol.hashCode());
		result = prime * result + ((nonTerminal == null) ? 0 : nonTerminal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RowColumnParsingTable other = (RowColumnParsingTable) obj;
		if (inputSymbol == null) {
			if (other.inputSymbol != null)
				return false;
		} else if (!inputSymbol.equals(other.inputSymbol))
			return false;
		if (nonTerminal == null) {
			if (other.nonTerminal != null)
				return false;
		} else if (!nonTerminal.equals(other.nonTerminal))
			return false;
		return true;
	}
	
	
}
