package exercise2.models;

public class ProductionVariable {

	private Variable variable;
	private Production production;
	
	public ProductionVariable(Variable variable, Production production) {
		super();
		this.variable = variable;
		this.production = production;
	}

	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

	public Production getProduction() {
		return production;
	}

	public void setProduction(Production production) {
		this.production = production;
	}


	@Override
	public boolean equals(Object obj) {
		ProductionVariable other = (ProductionVariable) obj;
		if (variable == null) {
			if (other.variable != null)
				return false;
		} else if (!variable.getValue().equals(other.variable.getValue()))
			return false;
		return true;
	}
	
}
