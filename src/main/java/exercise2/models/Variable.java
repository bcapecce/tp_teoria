package exercise2.models;

import java.util.HashSet;
import java.util.Set;

import exercise2.enums.VariableType;


public class Variable {
	
	public static final String VARIABLE = "X_{";
	public static final String TERMINAL = "[a-z]";
	public static final String NULL = "&";
	public static final String FINAL = "$";

	private String value;
	private VariableType type;
	private Set<ProductionVariable> firsts;
	private Set<ProductionVariable> nexts;

	public Variable(String value) {
		this.value = value;
		
		type = value.matches(TERMINAL) ? VariableType.TERMINAL : VariableType.VARIABLE;

	}
	public Variable(String value, VariableType type) {
		this.value = value;
		this.type = type;
		this.firsts = new HashSet<>();
		this.nexts = new HashSet<>();

	}
	
	public Set<ProductionVariable> getFirsts() {
		return firsts;
	}
	public void setFirsts(Set<ProductionVariable> firsts) {
		this.firsts = firsts;
	}
	
	public Set<ProductionVariable> getNexts() {
		return nexts;
	}
	public void setNexts(Set<ProductionVariable> nexts) {
		this.nexts = nexts;
	}
	public boolean isTerminal() {
		return VariableType.TERMINAL.equals(type);
	}
	
	public boolean isVariable() {
		return VariableType.VARIABLE.equals(type);
	}
	
	public boolean isNull() {
		return VariableType.NULL.equals(type);
	}
	
	public boolean isFinal() {
		return VariableType.FINAL.equals(type);
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value + ":" +type;
	}
	
	public String toStringValue() {
		return value;
	}
	public VariableType getType() {
		return type;
	}
	public void setType(VariableType type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variable other = (Variable) obj;
		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
}
