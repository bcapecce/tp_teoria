package exercise2.models;

public class Input {

	private String value;
	private String firstElement;
	
	
	public Input(String value) {
		super();
		this.value = value;
		firstElement = value.substring(0, 1);
	}
	public String getValue() {
		return value;
	}
	
	public String getFirstElement() {
		return firstElement;
	}
	public void setFirstElement(String firstElement) {
		this.firstElement = firstElement;
	}
	
	/**
	 * Este metodo se encarga de quitar un elemento del string actual y agregar el primer
	 * caracater al atributo firstElement
	 */
	public void advance() {
		value = value.substring(1, value.length());
		if(!value.isEmpty() || !value.equals("$")) {
			firstElement = value.substring(0, 1);

		}
		
	}
	
	public boolean isEmptyOrFinal() {
		return value.isEmpty() || value.equals("$");
	}
	
	public boolean isFinal() {
		return value.equals("$");
	}
	
	
}
