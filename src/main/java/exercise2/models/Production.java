package exercise2.models;

import java.util.List;


public class Production {

	private Variable partLeft;
	/**
	 * este atributo que corresponde a la parte derecha de una produccion
	 * esta dividida en una lista para hacer mas facil la obtencion de primeros
	 * y siguientes
	 */
	private List<Variable> partRigth;
	
	public Production(Variable partLeft, List<Variable> partRigth) {
		super();
		this.partLeft = partLeft;
		this.partRigth = partRigth;
	}

	public Variable getPartLeft() {
		return partLeft;
	}

	public void setPartLeft(Variable partLeft) {
		this.partLeft = partLeft;
	}

	public List<Variable> getPartRigth() {
		return partRigth;
	}

	public void setPartRigth(List<Variable> partRigth) {
		this.partRigth = partRigth;
	}

	public boolean existInPartRigth(Variable element) {
		return partRigth.contains(element);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((partLeft == null) ? 0 : partLeft.hashCode());
		result = prime * result + ((partRigth == null) ? 0 : partRigth.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Production other = (Production) obj;
		if (partLeft == null) {
			if (other.partLeft != null)
				return false;
		} else if (!partLeft.equals(other.partLeft))
			return false;
		if (partRigth == null) {
			if (other.partRigth != null)
				return false;
		} else if (!partRigth.equals(other.partRigth))
			return false;
		return true;
	}
	
	
	
}
