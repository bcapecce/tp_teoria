package exercise2;

import java.util.List;
import java.util.stream.Collectors;

import exercise2.exceptions.GrammarException;
import exercise2.grammar.Grammar;
import exercise2.manager.FileManager;
import exercise2.models.Production;
import exercise2.models.RowColumnParsingTable;
import exercise2.models.Variable;


public class Exercise2 {

	public static void main(String[] args) throws GrammarException {
		Grammar grammar = new Grammar();
		FileManager fileManager = new FileManager();
		List<String> productions = fileManager.readFile("grammar.txt");
		
		grammar.setProductions(productions);
		grammar.validateString("dabdadc");
		showElements(grammar);
		
	}
	
	
	public static void showElements(Grammar grammar) {
	
		System.out.println("PRODUCCIONES");
		grammar.getProductions().forEach(p -> System.out.println(p.getPartLeft().getValue()+"->"+p.getPartRigth().stream()
				.map(r -> r.getValue()).collect(Collectors.toList()).toString()));
		
		System.out.println("PRIMEROS");
		grammar.getVariables().entrySet().stream().forEach(p ->  System.out.println(p.getKey() +" -> "+ p.getValue().getFirsts()
				.stream().map(r -> r.getVariable().getValue()).collect(Collectors.toList()).toString()));
	
		System.out.println("SIGUIENTES");
		grammar.getVariables().entrySet().stream().filter(p -> p.getValue().isVariable())
		.forEach(p ->  System.out.println(p.getKey() +" -> "+ p.getValue()
		.getNexts().stream().map(r -> r.getVariable().getValue()).collect(Collectors.toList()).toString()));
	
		System.out.println("TABLA PARSING");		
		List<Variable> nonTerminals = grammar.getVariables().entrySet().stream().map(p -> p.getValue()).filter(p -> p.isVariable()).collect(Collectors.toList());
		List<Variable> inputSymbols = grammar.getVariables().entrySet().stream().map(p -> p.getValue()).filter(p -> !p.isVariable()).collect(Collectors.toList());
		 
		inputSymbols.stream().forEach(p -> System.out.print("                      "+p.getValue()));
		 System.out.println("        ");
		 nonTerminals.stream().forEach(p -> {
			 System.out.print(p.getValue());
			 inputSymbols.stream().forEach(r -> {
				 Production production = grammar.getParsingTable().get(new RowColumnParsingTable(r,p));
				 if(production!=null) {
					 System.out.print("            "+production.getPartLeft().getValue()+"->"+production.getPartRigth().stream().map(s -> s.getValue()).collect(Collectors.toList()).toString());

				 }else {
					 System.out.print("                      ");
				 }
				 
			 });
			 System.out.println("");

		 });
		
	}
	

}