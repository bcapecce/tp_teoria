package exercise2.manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import exercise2.mapper.Mapper;

public class FileManager {

	private Mapper mapper = new Mapper();
	
	
	public List<String> readFile(String fileName){
		
		List<String> lines = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))){
	
			String line = br.readLine();
			while (line != null) {
				lines.add(line);
				line = br.readLine();
			}
		}catch (FileNotFoundException e) {
			System.err.println("invalid file " + fileName);
		} catch (IOException e) {
			System.err.println("error reading file " + fileName);
		}
		return lines;
	}
}
