package exercise1.managers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FileManager {

	private List<String> alphabet;
	private int statesQuantity;
	private Set<String> finalStates;
	private List<String> transitionFunctions;

	public boolean readFile(String fileName) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
			String textAlphabet = br.readLine();
			loadAlphabet(textAlphabet);
			String textStates = br.readLine();
			this.statesQuantity = Integer.parseInt(textStates);
			String textFinalStates = br.readLine();
			loadFinalState(textFinalStates);
			transitionFunctions = new ArrayList<>();
			String line = br.readLine();
			while (line != null) {
				transitionFunctions.add(line);
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			System.err.println("invalid file " + fileName);
			return false;
		} catch (IOException e) {
			System.err.println("error reading file " + fileName);
			return false;
		}
		// Asegurar el cierre del fichero en cualquier caso
		finally {
			try {
				// Cerrar el fichero si se ha podido abrir
				if (br != null) {
					br.close();
				}
			} catch (Exception ex) {
				System.err.println("error closing file");
				return false;
			}
		}
		return true;
	}

	public List<String> getAlphabet() {
		return alphabet;
	}

	public int getStatesQuantity() {
		return statesQuantity;
	}

	public Set<String> getFinalStates() {
		return finalStates;
	}

	public List<String> getTransitionFunctions() {
		return transitionFunctions;
	}

	private void loadAlphabet(String line) {
		String alphabetWithoutSpaces = line.replace(" ", "");
		alphabet = Arrays.asList(alphabetWithoutSpaces.split(","));
	}

	private void loadFinalState(String line) {
		String finalStatesWithoutSpaces = line.replace(" ", "");
		finalStates = new HashSet<>(Arrays.asList(finalStatesWithoutSpaces.split(",")));
	}

}
