package exercise1.exceptions;

/**
 * Se dispara cuando un simbolo del string no pertenece al alfabeto
 *
 */
public class InvalidSymbolException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidSymbolException(String message) {
		super(message);
	}
}
