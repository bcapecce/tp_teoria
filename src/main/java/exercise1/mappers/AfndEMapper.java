package exercise1.mappers;

import exercise1.automatons.Afd;
import exercise1.automatons.Afnd;

/**
 * Esta clase es responsable de transformar un automata finito no deterministico
 * con transiciones epsilon a un automata finito deterministico para ello emplea
 * los siguientes mappers
 * 
 * AfndMapper: transforma un automata finito no deterministico con transiciones
 * epsilon a un automata finito no deterministico
 * 
 * AfdMapper: transforma un automata finito no deterministico a un transforma un
 * automata finito deterministico
 */
public class AfndEMapper {
	public static Afd afndEtoAfd(Afnd afndE) {

		AfdMapper afdMapper = new AfdMapper();
		AfndMapper afndMapper = new AfndMapper();

		Afnd afnd = afndMapper.afndEtoAfnd(afndE);
		Afd afd = afdMapper.afndtoAfd(afnd);

		return afd;
	}
}
