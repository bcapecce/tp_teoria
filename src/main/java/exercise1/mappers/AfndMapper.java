package exercise1.mappers;

import java.util.ArrayList;
import java.util.List;

import exercise1.automatons.Afnd;
import exercise1.models.State;
import exercise1.models.TransitionFunction;

/**
 * Esta clase es responsable de transforma un automata finito no deterministico
 * con transiciones epsilon a un automata finito no deterministico
 */
public class AfndMapper {

	/**
	 * transforma un automata finito no deterministico con transiciones epsilon a un
	 * automata finito no deterministico
	 *
	 * @param afndE automata finito no deterministico con transiciones epsilon.
	 * @return automata finito no deterministico.
	 * 
	 */
	public Afnd afndEtoAfnd(Afnd afndE) {
		List<String> alphabet = afndE.getAlphabet();
		List<State> states = afndE.getStates();
		List<State> newStates = new ArrayList<>();

		// Recorro todas las transciones y las copio al nuevo automata
		// Luego recorro los closures y agrego las nuevas transiciones
		// TODO validar transiciones duplicadas
		states.forEach(state -> {

			List<TransitionFunction> functions = state.getFunctions();
			functions.forEach(function -> {
				List<TransitionFunction> newTf = new ArrayList<>();
				State nextState = function.getNextState();
				String ch = function.getSymbol();
				newTf.add(new TransitionFunction(ch, nextState));

				List<State> closures = nextState.getClosures();
				closures.forEach(stateClosure -> {
					newTf.add(new TransitionFunction(ch, stateClosure));
				});

				State newState = new State(state.getName(), state.isFinal());
				newTf.forEach(tf -> {
					newState.addTransitionFunction(tf);
				});
				newStates.add(newState);
			});

		});
		Afnd afndE2 = new Afnd(alphabet, states);
		return afndE2;
	}
}
