package exercise1.mappers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import exercise1.automatons.Afd;
import exercise1.automatons.Afnd;
import exercise1.models.State;
import exercise1.models.TransitionFunction;

/**
 * Esta clase es responsable de transformar un automata finito no deterministico
 * sin transiciones epsilon a un automata finito deterministico
 */
public class AfdMapper {

	private Map<String, State> mapStates;

	/**
	 * Este método es el responsable de transformar un automata finito no
	 * deterministico sin transiciones epsilon a un automata finito deterministico
	 * 
	 * @param afnd automata finito no deterministico sin transiciones epsilon.
	 * 
	 * @return automata finito no deterministico.
	 */
	public Afd afndtoAfd(Afnd afnd) {
		List<String> alphabet = afnd.getAlphabet();
		List<State> states = new ArrayList<>(afnd.getStates());

		List<State> newStates = addCompoundStates(states);

		newStates = addTrapState(alphabet, newStates);

		Afd afd = new Afd(alphabet, newStates);

		return afd;
	}

	/**
	 * Agrega los estados compuestos que son los que representan a más de estado del
	 * afnd
	 * 
	 * @param states lista de estado del autómata.
	 * 
	 * @return lista de estados compuestos.
	 */
	private List<State> addCompoundStates(List<State> states) {
		boolean hasNewState = true;

		List<State> newStates = new ArrayList<State>();
		this.mapStates = states.stream().collect(Collectors.toMap(State::getName, Function.identity()));

		// recorro los estado y busco estado compuestos
		while (hasNewState) {
			hasNewState = false;
			for (State state : states) {
				Queue<State> tmpListStates = new LinkedList<>();
				tmpListStates.add(state);
				while (!tmpListStates.isEmpty()) {
					/*
					 * Se copia al estado actual el primer estado de las lista a procesar sin sus
					 * funciones de transición
					 */
					State currentState = tmpListStates.remove();

					/*
					 * Se verifica que no se haya procesado
					 */
					if (alReadyExists(newStates, currentState)) {
						continue;
					}
					/*
					 * Busco las funciones de transición considerando que el estado puede ser
					 * compuesto
					 */
					List<TransitionFunction> functions = getTransitionFunction(currentState);
					/*
					 * Genero a partir de las funciones de transición un mapa en donde la clave es
					 * el simbolo y el valor es una lista de estados destinos
					 */
					Map<String, List<State>> data = detectCompoundStates(functions);

					/*
					 * Creo los estados destino de las funciones de transición, los agrego a la
					 * lista de estado pendientes de procesar y genero la relación entre estos
					 * estados y el estado actual
					 */
					createNewStates(currentState, newStates, tmpListStates, data);

					/*
					 * Ahora si agrego el estado actual a la lista de nuevos estados
					 */
					newStates.add(currentState);
				}
			}

		}
		return newStates;
	}

	private List<TransitionFunction> getTransitionFunction(State currentState) {
		List<TransitionFunction> functions = null;
		/* agrego las funciones */
		if (currentState.isCompound()) {
			functions = new ArrayList<>();
			String[] stringNames = currentState.getName().split("-");
			for (String name : stringNames) {
				addFunctions(functions, this.mapStates.get(name).getFunctions());
			}
		} else {
			functions = this.mapStates.get(currentState.getName()).getFunctions();
		}
		return functions;
	}

	/**
	 * Agrega los estados trampa
	 * 
	 * @param states lista de estado del autómata.
	 * 
	 * @return lista de estado del autómata con estados trampa.
	 */
	private List<State> addTrapState(List<String> alphabet, List<State> states) {
		boolean addQt = false;

		// Creo estado tranpa
		State qt = new State("qt", false);
		for (State state : states) {
			List<String> alphabetTemp = new LinkedList<>(alphabet);
			var functions = state.getFunctions();
			for (TransitionFunction function : functions) {
				alphabetTemp.remove(function.getSymbol());
			}
			if (alphabetTemp.size() > 0) {
				for (String elemento : alphabetTemp) {
					state.addTransitionFunction(new TransitionFunction(elemento, qt));
					addQt = true;
				}
			}
		}
		if (addQt) {
			states.add(qt);
			for (String elemento : alphabet) {
				qt.addTransitionFunction(new TransitionFunction(elemento, qt));
			}
		}
		return states;
	}

	private void addFunctions(List<TransitionFunction> functions, List<TransitionFunction> newFunctions) {
		for (TransitionFunction newFunction : newFunctions) {
			if (functions.stream()
					.filter(f -> f.getSymbol().equals(newFunction.getSymbol())
							&& f.getNextState().getName().equals(newFunction.getNextState().getName()))
					.findAny().isEmpty()) {
				functions.add(newFunction);
			}
		}
	}

	private Map<String, List<State>> detectCompoundStates(List<TransitionFunction> functions) {
		Map<String, List<State>> data = new HashMap<String, List<State>>();
		List<State> bucket = null;
		for (TransitionFunction function : functions) {
			if (data.containsKey(function.getSymbol())) {
				bucket = data.get(function.getSymbol());
			} else {
				bucket = new ArrayList<>();
			}
			bucket.add(function.getNextState());
			data.put(function.getSymbol(), bucket);
		}
		return data;
	}

	private void createNewStates(State currentState, List<State> newStates, Queue<State> tmpNewStates,
			Map<String, List<State>> data) {
		currentState.getFunctions().clear();
		for (String c : data.keySet()) {
			if (data.get(c).size() > 1) {
				String stateName = getNewName(data.get(c));
				boolean isFinal = data.get(c).stream().anyMatch(s -> s.isFinal());
				if (!alReadyExistsStateName(newStates, tmpNewStates, stateName)) {
					State newState = new State(stateName, isFinal, true);
					tmpNewStates.add(newState);
					currentState.addTransitionFunction(new TransitionFunction(c, newState));
				}
			} else {
				String stateName = data.get(c).get(0).getName();
				boolean isFinal = data.get(c).get(0).isFinal();
				if (!alReadyExistsStateName(newStates, tmpNewStates, stateName)) {
					State newState = new State(stateName, isFinal, false);
					tmpNewStates.add(newState);
					currentState.addTransitionFunction(new TransitionFunction(c, newState));
				}
			}
		}
	}

	private String getNewName(List<State> states) {
		Set<String> setName = new HashSet<>();
		states.forEach(state -> {
			String[] names = state.getName().split("-");
			setName.addAll(Arrays.asList(names));
		});
		return setName.stream().sorted().collect(Collectors.joining("-"));
	}

	private boolean alReadyExistsStateName(List<State> newStates, Queue<State> tmpNewStates, String newStateName) {

		boolean exists = tmpNewStates.stream().filter(m -> m.getName().equals(newStateName)).findFirst().isPresent();

		if (!exists) {
			exists = newStates.stream().filter(m -> m.getName().equals(newStateName)).findFirst().isPresent();
		}

		return exists;
	}

	/**
	 * Verifica que el esatdo exista en la lista de nuevos estado, en el caso de
	 * estado compuestos, tiene que verificar que todos los componentes del estado
	 * se encuentre en alguno de los estados de la nueva lista
	 * 
	 * @param states lista de estado del autómata.
	 * 
	 * @return lista de estados compuestos.
	 */
	private boolean alReadyExists(List<State> newStates, State state) {
		boolean exists = false;
		String[] originalNameArray = state.getName().split("-");
		for (State newState : newStates) {
			exists = true;
			Set<String> statesNameSet = new HashSet<>(Arrays.asList(newState.getName().split("-")));
			for (String originalStatesName : originalNameArray) {
				if (!statesNameSet.contains(originalStatesName)) {
					exists = false;
					break;
				}
			}
			if (exists) {
				break;
			}
		}
		return exists;
	}

}
