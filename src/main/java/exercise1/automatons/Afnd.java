package exercise1.automatons;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import exercise1.exceptions.InvalidSymbolException;
import exercise1.models.State;

/**
 * Esta clase representa un automata finito no deterministico con o sin
 * transiciones epsilon
 */
public class Afnd {
	private List<String> alphabet;
	private List<State> states;

	public Afnd(List<String> alphabet, List<State> states) {
		this.alphabet = alphabet;
		this.states = states;
	}

	public List<String> getAlphabet() {
		return alphabet;
	}

	public List<State> getStates() {
		return states;
	}

	/**
	 * Procesa un string
	 * 
	 * @param w string a procesar.
	 * 
	 * @return devuelve verdadero o falso seg´un si el string pertenece o no al
	 *         lenguaje.
	 * @exception InvalidSymbolException Se dispara si un simbolo del string no
	 *                                   pertenece al alfabeto
	 */
	public boolean procesar(String w) {

		List<String> list = getStringList(w);

		var currentStates = Arrays.asList(states.get(0));

		for (String element : list) {

			if (!alphabet.contains(element)) {
				throw new InvalidSymbolException(
						String.format("The symbol %s is not defined in the alphabet", element));
			}

			var stateList = currentStates.stream().flatMap(state -> getNextStates(state, element).stream())
					.collect(Collectors.toList());

			if (stateList.isEmpty()) {
				return false;
			}
			currentStates = stateList;
		}

		if (currentStates.stream().anyMatch(s -> s.isFinal())) {
			return true;
		}
		return false;

	}

	private List<State> getNextStates(State currentState, String element) {

		var nextStates = currentState.getFunctions().stream().filter(tf -> tf.getSymbol().equals(element))
				.map(tf -> tf.getNextState()).collect(Collectors.toList());

		var newStatesFromClosures = nextStates.stream().flatMap(m -> m.getClosures().stream())
				.collect(Collectors.toList());

		nextStates.addAll(newStatesFromClosures);

		return nextStates;
	}

	private List<String> getStringList(String w) {
		var array = w.split("");
		if (array.length == 1 && array[0] == "") {
			return new ArrayList<String>();
		}
		return Arrays.asList(array);
	}

}
