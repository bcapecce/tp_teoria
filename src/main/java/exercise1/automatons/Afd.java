package exercise1.automatons;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import exercise1.exceptions.InvalidSymbolException;
import exercise1.models.State;

/**
 * Esta clase representa un automata finito deterministico
 */
public class Afd {

	private List<String> alphabet;
	private List<State> states;

	public Afd(List<String> alphabet, List<State> states) {
		this.alphabet = alphabet;
		this.states = states;
	}

	public List<String> getAlphabet() {
		return alphabet;
	}

	/**
	 * Procesa un string
	 * 
	 * @param w string a procesar.
	 * 
	 * @return devuelve verdadero o falso seg´un si el string pertenece o no al
	 *         lenguaje.
	 * @exception InvalidSymbolException Se dispara si un simbolo del string no
	 *                                   pertenece al alfabeto
	 */
	public boolean procesar(String w) {

		List<String> list = getStringList(w);

		var currentState = states.get(0);
		for (String symbol : list) {
			if (!alphabet.contains(symbol)) {
				throw new InvalidSymbolException(String.format("The symbol %s is not defined in the alphabet", symbol));
			}
			currentState = getNextState(currentState, symbol);
			if (currentState == null) {
				return false;
			}
		}
		if (currentState.isFinal()) {
			return true;
		}
		return false;
	}

	private State getNextState(State currentState, String element) {
		return currentState.getFunctions().stream().filter(tf -> tf.getSymbol().equals(element)).findFirst()
				.map(tf -> tf.getNextState()).orElse(null);
	}

	private List<String> getStringList(String w) {
		var array = w.split("");
		if (array.length == 1 && array[0] == "") {
			return new ArrayList<String>();
		}
		return Arrays.asList(array);
	}
}
