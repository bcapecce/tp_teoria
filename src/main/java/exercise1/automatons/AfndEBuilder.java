package exercise1.automatons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import exercise1.managers.FileManager;
import exercise1.models.State;
import exercise1.models.TransitionFunction;

/**
 * Esta clase representa builder encargado de construir un automata finito no
 * deterministico con transiciones epsilon a partir de un archivo con un formato
 */
public class AfndEBuilder {

	private FileManager fileManager;

	public AfndEBuilder(FileManager fileManager) {
		this.fileManager = fileManager;
	}

	/**
	 * Contruye un automata automata finito no determinístico
	 * 
	 */
	public Afnd build() {

		Set<String> finalStates = fileManager.getFinalStates();
		int quantity = fileManager.getStatesQuantity();

		Map<String, State> states = new HashMap<String, State>();
		for (int i = 0; i < quantity; i++) {
			String stateName = Integer.toString(i);
			State s = new State(stateName, finalStates.contains(stateName));
			states.put(stateName, s);
		}
		fileManager.getTransitionFunctions().forEach(line -> {
			completeStateFromLine(states, line);
		});

		Afnd afndE = new Afnd(fileManager.getAlphabet(), new ArrayList<State>(states.values()));
		return afndE;
	}

	private void completeStateFromLine(Map<String, State> states, String line) {
		String[] arrays = line.split(",");
		String[] arrays2 = arrays[1].split("->");

		String stateName = arrays[0].replace(" ", "");
		String ch = arrays2[0].replace(" ", "");
		String nextStateName = arrays2[1].replace(" ", "");
		State state = states.get(stateName);
		if ("E".equals(ch)) {
			state.addClosure(states.get(nextStateName));
		} else {
			TransitionFunction tf = new TransitionFunction(ch, states.get(nextStateName));
			state.addTransitionFunction(tf);
		}
	}

}
