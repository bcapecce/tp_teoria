package exercise1;

import exercise1.automatons.Afd;
import exercise1.automatons.Afnd;
import exercise1.automatons.AfndEBuilder;
import exercise1.managers.FileManager;
import exercise1.mappers.AfndEMapper;

public class Exercise1 {

	public static void main(String[] args) {

		if (args.length < 1) {
			System.err.println("Se debe especificar el archivo de reglas");
			return;
		}

		// Cargo el automata
		FileManager fl = new FileManager();

		fl.readFile(args[0]);

		AfndEBuilder builder = new AfndEBuilder(fl);
		Afnd afndE = builder.build();

		// lo pase a un automata finito deterministico
		Afd afd = AfndEMapper.afndEtoAfd(afndE);

		// Proceso
		afd.procesar("a");

	}

}