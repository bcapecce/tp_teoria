package exercise1.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase representa un estado de un automata
 */
public class State {
	private String name;
	private List<TransitionFunction> functions;
	private List<State> closures;
	private boolean isFinal;
	private boolean isCompound;

	public State(String name, boolean isFinal) {
		build(name, isFinal, false);
	}

	public State(String name, boolean isFinal, boolean isCompound) {
		build(name, isFinal, isCompound);
	}

	private void build(String name, boolean isFinal, boolean isCompound) {
		this.name = name;
		this.isFinal = isFinal;
		this.isCompound = isCompound;
		this.functions = new ArrayList<TransitionFunction>();
		this.closures = new ArrayList<State>();

	}

	public String getName() {
		return name;
	}

	public void addTransitionFunction(TransitionFunction tf) {
		functions.add(tf);
	}

	public void addClosure(State state) {
		closures.add(state);
	}

	public boolean isFinal() {
		return isFinal;
	}

	public List<TransitionFunction> getFunctions() {
		return functions;
	}

	public List<State> getClosures() {
		return closures;
	}

	public boolean isCompound() {
		return isCompound;
	}
}
