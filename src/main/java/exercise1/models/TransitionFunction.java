package exercise1.models;

/**
 * Esta clase representa una función de transición de un automata
 */
public class TransitionFunction {
	private String symbol;
	private State nextState;

	public TransitionFunction(String val, State nextState) {
		this.symbol = val;
		this.nextState = nextState;
	}

	public String getSymbol() {
		return symbol;
	}

	public State getNextState() {
		return nextState;
	}

}
