package exercise3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class CintaTest {

	@Test
	void testGetSimboloActual() {
		Cinta cinta = new Cinta("B", Arrays.asList("0", "1"), "01");
		assertEquals("0", cinta.getSimboloActual());

		cinta.mover(Direccion.DERECHA);
		assertEquals("1", cinta.getSimboloActual());

	}

	@Test
	void testPisar() {

		Cinta cinta2 = new Cinta("B", Arrays.asList("0", "1"), "01");
		// System.out.println(cinta2.getPosicionActual() +""+cinta2.getSimboloActual());

		// se pisa el simbolo 0 de la primera posicion por 1
		cinta2.pisar("1");
		// System.out.println(cinta2.getPosicionActual() +""+cinta2.getSimboloActual());
		assertEquals("1", cinta2.getSimboloActual());

		// se pisa el simbolo actual de la primera posicion por B
		cinta2.pisar("B");
		// System.out.println(cinta2.getPosicionActual() +""+cinta2.getSimboloActual());
		assertEquals("B", cinta2.getSimboloActual());

		cinta2.pisar("B");
		assertNotNull(cinta2.getSimboloActual());

	}

	@Test
	void testMover() {

		Cinta cinta3 = new Cinta("B", Arrays.asList("0", "1"), "0110");
		// System.out.println(cinta3.getPosicionActual() +""+cinta3.getSimboloActual());

		cinta3.mover(Direccion.DERECHA);
		// System.out.println(cinta3.getPosicionActual() +""+cinta3.getSimboloActual());
		assertEquals(1, cinta3.getPosicionActual());

		cinta3.mover(Direccion.DERECHA);
		// System.out.println(cinta3.getPosicionActual() +""+cinta3.getSimboloActual());
		assertEquals(2, cinta3.getPosicionActual());

		cinta3.mover(Direccion.IZQUIERDA);
		// System.out.println(cinta3.getPosicionActual() +""+cinta3.getSimboloActual());
		assertEquals(1, cinta3.getPosicionActual());

		cinta3.mover(Direccion.IZQUIERDA);
		// System.out.println(cinta3.getPosicionActual() +""+cinta3.getSimboloActual());
		assertEquals(0, cinta3.getPosicionActual());

	}

}
