package exercise3;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class MaquinaTest {
	@Test
	public void testTerminaEnEstadoFinal() {
		Maquina maquina = setUp(true);
		assertTrue(maquina.correr());
	}

	@Test
	public void testPorDetencion() {
		Maquina maquina = setUp(false);
		assertFalse(maquina.correr());
	}

	private Maquina setUp(boolean estadoFinal) {
		Cinta cinta = new Cinta("B", Arrays.asList("0", "1"), "0110");
		Map<ReglaArgumento, ReglaAplicar> funcTransic = new HashMap<>();
		funcTransic.put(new ReglaArgumento("q", "0"), new ReglaAplicar("q", "0", Direccion.DERECHA));
		funcTransic.put(new ReglaArgumento("q", "1"), new ReglaAplicar("f", "0", Direccion.DERECHA));
		funcTransic.put(new ReglaArgumento("q", "B"), new ReglaAplicar("q", "1", Direccion.IZQUIERDA));

		List<String> estadosFinal = new ArrayList<>();
		if (estadoFinal) {
			estadosFinal.add("f");
		}
		return new Maquina(cinta, funcTransic, "q", estadosFinal, Arrays.asList("0", "1"));
	}
}
