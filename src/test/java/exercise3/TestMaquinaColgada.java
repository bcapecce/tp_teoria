package exercise3;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class TestMaquinaColgada {
	@Test
	public void testSecuelgaADerecha() {
		Cinta cinta = new Cinta("B", Arrays.asList("0", "1"), "0110");
		Map<ReglaArgumento, ReglaAplicar> funcTransic = new HashMap<>();
		funcTransic.put(new ReglaArgumento("q", "0"), new ReglaAplicar("q", "0", Direccion.DERECHA));
		funcTransic.put(new ReglaArgumento("q", "1"), new ReglaAplicar("q", "0", Direccion.DERECHA));
		funcTransic.put(new ReglaArgumento("q", "B"), new ReglaAplicar("q", "1", Direccion.DERECHA));

		List<String> estadosFinal = new ArrayList<>();

		estadosFinal.add("f");

		Maquina maquina = new Maquina(cinta, funcTransic, "q", estadosFinal, Arrays.asList("0", "1"));
		assertFalse(maquina.correr());
	}

	@Test
	public void testHaceUnBucle() {
		Cinta cinta = new Cinta("B", Arrays.asList("0", "1"), "0110");
		Map<ReglaArgumento, ReglaAplicar> funcTransic = new HashMap<>();
		funcTransic.put(new ReglaArgumento("q", "0"), new ReglaAplicar("q", "0", Direccion.DERECHA));
		funcTransic.put(new ReglaArgumento("q", "1"), new ReglaAplicar("q", "1", Direccion.IZQUIERDA));
		funcTransic.put(new ReglaArgumento("q", "B"), new ReglaAplicar("q", "1", Direccion.DERECHA));

		List<String> estadosFinal = new ArrayList<>();

		estadosFinal.add("f");

		Maquina maquina = new Maquina(cinta, funcTransic, "q", estadosFinal, Arrays.asList("0", "1"));
		assertFalse(maquina.correr());
	}
}
