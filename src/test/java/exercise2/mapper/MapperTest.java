package exercise2.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import exercise2.mapper.Mapper;
import exercise2.models.Production;
import exercise2.models.Variable;

public class MapperTest {

	private Mapper mapper = new Mapper();
	
	@Test
	public void whenMapStringToProductionIsReturnProduction() {
		
		assertThat(mapper.mapStringToProduction("X_{1} -> aX_{3}&")).hasFieldOrPropertyWithValue("partLeft", new Variable("X_{1}"))
		.extracting("partRigth").asList().extracting("value").containsOnly("a","X_{3}","&");
		
	}
	
	@Test
	public void whenMapStringsToProductionIsReturnProduction() {
		
		List<String> lines = new ArrayList<>();
		lines.add("X_{1} -> aX_{3}&");
		lines.add("X_{1} -> b");
		List<Production> productions = mapper.mapStringsToProductions(lines);
		
		assertThat(productions.get(0)).hasFieldOrPropertyWithValue("partLeft", new Variable("X_{1}"))
		.extracting("partRigth").asList().extracting("value").containsOnly("a","X_{3}","&");
		
		assertThat(productions.get(1)).hasFieldOrPropertyWithValue("partLeft", new Variable("X_{1}"))
		.extracting("partRigth").asList().extracting("value").containsOnly("b");
		
	}
}
