package exercise2.grammar;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import exercise2.enums.VariableType;
import exercise2.exceptions.GrammarException;
import exercise2.grammar.Grammar;
import exercise2.models.RowColumnParsingTable;
import exercise2.models.Variable;

public class GrammarTest {

	private Grammar grammar = new Grammar();
	private Variable x1;
	private Variable x2;
	private Variable x3;
	private Variable a;
	private Variable b;
	private Variable c;
	private Variable d;
	private Variable fin;
	private Variable nulo;
	List<String> lines = new ArrayList<>();

	
	@Before
	public void setUp() {

		lines.add("X_{1} -> X_{2}a");
		lines.add("X_{1} ->&");
		lines.add("X_{2} -> bX_{1}c");
		lines.add("X_{2} -> d");
		lines.add("X_{3} -> aX_{2}X_{3}");
		lines.add("X_{3} -> &");

		//primeros
		//x1 -> b,d,&
		//x2 -> b,d
		//x3 -> a,&
		//siguientes
		//x1 -> c,$
		//x2 -> a,$
		//x3 -> $
		
		//					a					b					c					d						$
		//	x1								X_{1} -> X_{2}a		X_{1} ->&			X_{1} -> X_{2}a			X_{1} ->&	
		//  x2								X_{2} -> bX_{1}c						X_{2} -> d
		//  x3		X_{3} -> aX_{2}X_{3}																	X_{3} -> &
		x1 = new Variable("X_{1}");
		x2 = new Variable("X_{2}");
		x3 = new Variable("X_{3}");
		a = new Variable("a");
		b = new Variable("b");
		c = new Variable("c");
		d = new Variable("d");
		fin = new Variable("$", VariableType.FINAL);
		nulo = new Variable("&", VariableType.NULL);
	}
	@Test
	public void whenParsingTableIsOk() throws GrammarException {
		
		grammar.setProductions(lines);	
		//columna a
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(a,x1))).isNull();
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(a,x2))).isNull();
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(a,x3)))
			.hasFieldOrPropertyWithValue("partLeft", x3)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(a,x2,x3));
		
		//columna b
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(d,x1)))
			.hasFieldOrPropertyWithValue("partLeft", x1)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(x2,a));
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(b,x2)))
			.hasFieldOrPropertyWithValue("partLeft", x2)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(b,x1,c));
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(b,x3))).isNull();

		//columna c
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(c,x1)))
			.hasFieldOrPropertyWithValue("partLeft", x1)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(nulo));
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(c,x2))).isNull();
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(c,x3))).isNull();
		
		//columna d
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(d,x1)))
			.hasFieldOrPropertyWithValue("partLeft", x1)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(x2,a));
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(d,x2)))
			.hasFieldOrPropertyWithValue("partLeft", x2)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(d));
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(d,x3))).isNull();
		
		//columna d
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(fin,x1)))
			.hasFieldOrPropertyWithValue("partLeft", x1)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(nulo));	
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(fin,x2))).isNull();
		assertThat(grammar.getParsingTable().get(new RowColumnParsingTable(fin,x3)))
			.hasFieldOrPropertyWithValue("partLeft", x3)
			.hasFieldOrPropertyWithValue("partRigth", Arrays.asList(nulo));	

	}
	
	@Test
	public void whenValidateStringIsok() throws GrammarException {
		grammar.setProductions(lines);	
		assertThat(grammar.validateString("bca")).isTrue();
		
	}
	
	@Test
	public void whenValidateStringFail() throws GrammarException {
		grammar.setProductions(lines);	
		assertThat(grammar.validateString("bcca")).isFalse();
		
	}
	
	
}
