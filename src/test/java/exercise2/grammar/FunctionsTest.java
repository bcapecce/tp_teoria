package exercise2.grammar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import exercise2.Commons;
import exercise2.grammar.Functions;
import exercise2.models.Production;
import exercise2.models.ProductionVariable;
import exercise2.models.Variable;

public class FunctionsTest {

	private Commons commons = new Commons();
	private List<Variable> variables = new ArrayList<>();
	private List<Production> productions = commons.buildProductions();
	private Functions functions = new Functions();

	
	@Before
	public void setUp() {
		functions.setProductions(productions);

		variables.add(productions.get(0).getPartLeft());
		variables.add(productions.get(1).getPartLeft());
		variables.add(productions.get(3).getPartLeft());
		variables.add(productions.get(5).getPartLeft());
		variables.add(productions.get(6).getPartLeft());
		variables.add(new Variable("a"));
		variables.add(new Variable("&"));
		variables.add(new Variable("b"));
		variables.add(new Variable("c"));
		variables.add(new Variable("d"));
		variables.add(new Variable("$"));

		
		productions.add(commons.buildProduction(variables.get(1),Arrays.asList(variables.get(2))));

	}
	
	@Test
	public void whenFindFirstIsOk() {
		System.out.println("*******whenFindFirstIsOk");

		Set<ProductionVariable> productionVariables = functions.findFirsts(variables.get(1),variables);
		
		assertThat(productionVariables).extracting("variable").extracting("value").containsOnly("a","&","b","d");
		assertThat(productionVariables).extracting("production")
		.containsOnly(productions.get(1), productions.get(2),productions.get(7), productions.get(7));

	}
	
	@Test
	public void whenFindFirstAllOk() {
		System.out.println("*******whenFindFirstAllOk");


		functions.findFirsts(variables);

		assertThat(functions.getVariables().get("X_{1}").getFirsts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("d","b");
		
		assertThat(functions.getVariables().get("X_{2}").getFirsts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("d","b");
		
		assertThat(functions.getVariables().get("X_{3}").getFirsts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("d","b","a","&");
		
		assertThat(functions.getVariables().get("a").getFirsts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("a");
		

	}
	
	@Test
	public void whenfindNextsOfElementIsOkFirstRule() {
		System.out.println("*******whenfindNextsOfElementIsOkFirstRule***************");
		Variable x1 = variables.get(0);
		functions.findFirsts(variables);
		
		assertThat(functions.findNextsOfElement(x1).stream().collect(Collectors.toList()))
		.extracting("variable").extracting("value").containsOnly("c");
	}
	@Test
	public void whenfindNextsOfElementIsOkFirstRuleTwo() {
		System.out.println("*******whenfindNextsOfElementIsOkFirstRuleTwo***************");
		Variable x4 = variables.get(3);
		
		functions.findFirsts(variables);
		
		assertThat(functions.findNextsOfElement(x4).stream().collect(Collectors.toList()))
		.extracting("variable").extracting("value").containsOnly("a");
	}

	@Test
	public void whenfindNextsOfElementIsOkSecondRule() {
		System.out.println("*******whenfindNextsOfElementIsOkSecondRule********");

		Variable x3 = variables.get(1);
		Variable x1 = variables.get(0);

		functions.findFirsts(variables);

		functions.getVariables().get(x1.getValue()).setNexts(functions.findNextsOfElement(x1));

		assertThat(functions.findNextsOfElement(x3).stream().collect(Collectors.toList()))
		.extracting("variable").extracting("value").containsOnly("c");

	}
	@Test
	public void whenfindNextsOfElementIsOkSecondRuleWithTwo() {
		System.out.println("*******whenfindNextsOfElementIsOkSecondRuleWithTwo***************");


		Variable x2 = variables.get(2);
		Variable x3 = variables.get(1);
		functions.findFirsts(variables);
		functions.getVariables().get("X_{3}").setNexts(functions.findNextsOfElement(x3));
		assertThat(functions.findNextsOfElement(x2).stream().collect(Collectors.toList()))
		.extracting("variable").extracting("value").containsOnly("c");

	}
	
	
	@Test
	public void whenfindAllNextsIsOk() {
		System.out.println("*******whenfindNextsOfElementIsOkSecondRuleWithTwo***************");
		Variable x1 = variables.get(0);
		Variable x4 = variables.get(3);
		Variable x3 = variables.get(1);
		Variable x2 = variables.get(2);

		functions.findFirsts(variables);
		functions.findAllNexts();

		
		assertThat(functions.getVariables().get(x1.getValue()).getNexts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("c","$");
		
		assertThat(functions.getVariables().get(x4.getValue()).getNexts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("a","$");
		
		assertThat(functions.getVariables().get(x2.getValue()).getNexts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("c","$");
		
		assertThat(functions.getVariables().get(x3.getValue()).getNexts().stream().collect(Collectors.toList()))
		.asList().extracting("variable").extracting("value").containsOnly("c","$");
		

	}


}
