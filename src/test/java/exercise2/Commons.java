package exercise2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import exercise2.enums.VariableType;
import exercise2.models.Production;
import exercise2.models.Variable;


public class Commons {

	private static final String VARIABLE = "X_{#}";

	public List<Variable> buildVariables(){
		return Arrays.asList(buildVariable("1"),buildVariable("2"),buildVariable("3"));
	}
	
	public List<Variable> buildVariables(String value1, String value2){
		return Arrays.asList(buildVariable(value1),buildVariable(value2));	
	}
	public List<Variable> buildVariables(String value1, String value2, String value3){
		return Arrays.asList(buildVariable(value1),buildVariable(value2), buildVariable(value3));	
	}
	
	public Variable buildVariable(String value, VariableType type) {
		return new Variable(VARIABLE.replace("#", value),type);
	}
	
	public Variable buildVariable(String value) {
		return new Variable(VARIABLE.replace("#", value),VariableType.VARIABLE);
	}
	
	public Variable buildTerminal(String value) {
		return new Variable( value,VariableType.TERMINAL);
	}
	
	public Variable buildNull() {
		return new Variable("&",VariableType.NULL);
	}
	
	public Variable buildFinal() {
		return new Variable("$",VariableType.FINAL);
	}
	
	public Production buildProduction(Variable valueLeft, List<Variable> variables) {
		Production production = new Production(valueLeft, variables);
		showProduction(production);
		return production;
	}
	public Production buildProduction(String valueLeft, String valueRight1, String valueRight2, String valueRight3) {
		return new Production(buildVariable(valueLeft), buildVariables(valueRight1, valueRight2));
	}
	
	public List<Production> buildProductions(){
		List<Production> productions = new ArrayList<>();
		//0
		Variable var = buildVariable("1");
		List<Variable> variables = buildVariables("2","3");
		productions.add(buildProduction(var, variables));
		//1
		var = buildVariable("3");
		variables = new ArrayList<>();
		variables.add(buildTerminal("a"));
		variables.addAll(buildVariables("2","3"));
		
		productions.add(buildProduction(var, variables));

		//2
		variables= new ArrayList<>();
		variables.add(buildNull());
		productions.add(buildProduction(var, variables));

		//3
		var = buildVariable("2");
		variables = new ArrayList<>();
		variables.add(buildTerminal("b"));
		variables.add(buildVariable("1"));
		variables.add(buildTerminal("c"));
		productions.add(buildProduction(var, variables));

		//4
		variables = new ArrayList<>();
		variables.add(buildTerminal("d"));
		productions.add(buildProduction(var, variables));
		
		//5
		var = buildVariable("4");
		variables = new ArrayList<>();
		variables.add(buildTerminal("b"));
		variables.add(buildVariable("4"));
		variables.add(buildVariable("5"));
		productions.add(buildProduction(var, variables));
		//6
		var = buildVariable("5");
		variables = new ArrayList<>();
		variables.add(buildTerminal("a"));
		productions.add(buildProduction(var, variables));
		return productions;
	}
	
	public void showProduction(Production p) {
		System.out.println(p.getPartLeft().getValue()+"->"+
				p.getPartRigth().stream()
				.map(r -> r.getValue())
				.collect(Collectors.toList())
				.toString());

	}
	
	
	
	
}
