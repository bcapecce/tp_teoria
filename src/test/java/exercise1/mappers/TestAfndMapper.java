package exercise1.mappers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import exercise1.automatons.Afd;
import exercise1.automatons.Afnd;
import exercise1.models.State;
import exercise1.models.TransitionFunction;

public class TestAfndMapper {

	/**
	 * Este test busca probar el mapeo de un automata no deterministico con
	 * transiciones epsilon sencillo
	 */
	@Test
	void testMapperToAfnd() {

		AfndMapper mapper = new AfndMapper();

		State q0 = new State("q0", false);
		State q1 = new State("q1", false);
		State q2 = new State("q2", true);

		q0.addTransitionFunction(new TransitionFunction("0", q1));
		q1.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("1", q2));
		q1.addClosure(q2);

		Afnd afndE = new Afnd(Arrays.asList("0", "1"), Arrays.asList(q0, q1, q2));
		Afnd afnd = mapper.afndEtoAfnd(afndE);

		assertTrue(afnd.procesar("0"));
	}

	/**
	 * Este test busca probar el mapeo de un automata no deterministico sencillo
	 */
	@Test
	void testMapperToAfd() {

		AfdMapper mapper = new AfdMapper();

		State q0 = new State("q0", false);
		State q1 = new State("q1", true);
		State q2 = new State("q2", false);

		q0.addTransitionFunction(new TransitionFunction("0", q1));
		q0.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("1", q0));

		Afnd afnd = new Afnd(Arrays.asList("0", "1"), Arrays.asList(q0, q1, q2));
		Afd afd = mapper.afndtoAfd(afnd);

		assertTrue(afd.procesar("0"));
		assertEquals("different size", 2, afd.getAlphabet().size());
	}

	/**
	 * Este test busca probar el mapeo de automata finito no deterministico a
	 * automata finito deterministico que se dio en clase y que se encuentra en la
	 * presentación
	 */
	@Test
	void testMapperToAfd2() {

		AfdMapper mapper = new AfdMapper();

		State q1 = new State("1", false);
		State q2 = new State("2", false);
		State q3 = new State("3", false);
		State q4 = new State("4", false);
		State q5 = new State("5", false);
		State q6 = new State("6", false);
		State q7 = new State("7", false);
		State q8 = new State("8", false);
		State q9 = new State("9", true);

		q1.addTransitionFunction(new TransitionFunction("r", q2));
		q1.addTransitionFunction(new TransitionFunction("r", q4));
		q1.addTransitionFunction(new TransitionFunction("b", q5));

		q2.addTransitionFunction(new TransitionFunction("r", q4));
		q2.addTransitionFunction(new TransitionFunction("r", q6));
		q2.addTransitionFunction(new TransitionFunction("b", q1));
		q2.addTransitionFunction(new TransitionFunction("b", q3));
		q2.addTransitionFunction(new TransitionFunction("b", q5));

		q3.addTransitionFunction(new TransitionFunction("r", q2));
		q3.addTransitionFunction(new TransitionFunction("r", q6));
		q3.addTransitionFunction(new TransitionFunction("b", q5));

		q4.addTransitionFunction(new TransitionFunction("r", q2));
		q4.addTransitionFunction(new TransitionFunction("r", q8));
		q4.addTransitionFunction(new TransitionFunction("b", q1));
		q4.addTransitionFunction(new TransitionFunction("b", q5));
		q4.addTransitionFunction(new TransitionFunction("b", q7));

		q5.addTransitionFunction(new TransitionFunction("r", q2));
		q5.addTransitionFunction(new TransitionFunction("r", q4));
		q5.addTransitionFunction(new TransitionFunction("r", q6));
		q5.addTransitionFunction(new TransitionFunction("r", q8));
		q5.addTransitionFunction(new TransitionFunction("b", q1));
		q5.addTransitionFunction(new TransitionFunction("b", q3));
		q5.addTransitionFunction(new TransitionFunction("b", q7));
		q5.addTransitionFunction(new TransitionFunction("b", q9));

		q6.addTransitionFunction(new TransitionFunction("r", q2));
		q6.addTransitionFunction(new TransitionFunction("r", q8));
		q6.addTransitionFunction(new TransitionFunction("b", q3));
		q6.addTransitionFunction(new TransitionFunction("b", q5));
		q6.addTransitionFunction(new TransitionFunction("b", q9));

		q7.addTransitionFunction(new TransitionFunction("r", q4));
		q7.addTransitionFunction(new TransitionFunction("r", q8));
		q7.addTransitionFunction(new TransitionFunction("b", q5));

		q8.addTransitionFunction(new TransitionFunction("r", q4));
		q8.addTransitionFunction(new TransitionFunction("r", q6));
		q8.addTransitionFunction(new TransitionFunction("b", q5));
		q8.addTransitionFunction(new TransitionFunction("b", q7));
		q8.addTransitionFunction(new TransitionFunction("b", q9));

		q9.addTransitionFunction(new TransitionFunction("r", q6));
		q9.addTransitionFunction(new TransitionFunction("r", q8));
		q9.addTransitionFunction(new TransitionFunction("b", q5));

		Afnd afnd = new Afnd(Arrays.asList("r", "b"), Arrays.asList(q1, q2, q3, q4, q5, q6, q7, q8, q9));

		Afd afd = mapper.afndtoAfd(afnd);

		assertTrue(afd.procesar("rrb"));
		assertEquals("different size", 2, afd.getAlphabet().size());
	}

}
