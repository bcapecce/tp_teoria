package exercise1.automatons;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import exercise1.managers.FileManager;

public class TestAfndEBuilder {
	private FileManager fileManager;

	@Test
	void testSimpleAfnd() {

		fileManager = Mockito.mock(FileManager.class);

		Mockito.when(fileManager.getAlphabet()).thenReturn(Arrays.asList("0", "1"));
		Mockito.when(fileManager.getStatesQuantity()).thenReturn(3);
		Mockito.when(fileManager.getFinalStates()).thenReturn(new HashSet<>(Arrays.asList("2")));
		Mockito.when(fileManager.getTransitionFunctions())
				.thenReturn(Arrays.asList("0, 0 -> 1", "1, 0 -> 2", "2, 0 -> 2", "2, 1 -> 2"));

		AfndEBuilder builder = new AfndEBuilder(fileManager);
		Afnd afndE = builder.build();

		assertTrue(afndE.procesar("000"));
	}

}
