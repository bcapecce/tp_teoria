package exercise1.automatons;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import exercise1.exceptions.InvalidSymbolException;
import exercise1.models.State;
import exercise1.models.TransitionFunction;

public class TestAfnd {

	/**
	 * Este test busca probar el funcionamiento de un automata finito que verifica
	 * que el string comience con 00
	 */
	@Test
	void testSimpleAfnd() {

		State q0 = new State("q0", false);
		State q1 = new State("q1", false);
		State q2 = new State("q2", true);

		q0.addTransitionFunction(new TransitionFunction("0", q1));
		q1.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("1", q2));

		Afnd afndE = new Afnd(Arrays.asList("0", "1"), Arrays.asList(q0, q1, q2));

		assertTrue(afndE.procesar("000"));
		assertTrue(afndE.procesar("001"));
		assertTrue(afndE.procesar("000101010101"));
		assertTrue(afndE.procesar("001010101001"));
		assertFalse(afndE.procesar("100"));
		assertFalse(afndE.procesar("010"));
		assertFalse(afndE.procesar("110"));
	}

	/**
	 * Este test busca probar el funcionamiento de un automata finito que verifica
	 * que el string comience con 00, la diferencia con el anterior es que el estado
	 * q0 posee dos transciones con el simbolo 0
	 */
	@Test
	void testAfndWith2Tf() {

		State q0 = new State("q0", false);
		State q1 = new State("q1", false);
		State q2 = new State("q2", true);

		q0.addTransitionFunction(new TransitionFunction("0", q0));
		q0.addTransitionFunction(new TransitionFunction("0", q1));
		q1.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("1", q2));

		Afnd afndE = new Afnd(Arrays.asList("0", "1"), Arrays.asList(q0, q1, q2));

		assertTrue(afndE.procesar("000"));
		assertTrue(afndE.procesar("001"));
		assertTrue(afndE.procesar("000101010101"));
		assertTrue(afndE.procesar("001010101001"));
		assertFalse(afndE.procesar("100"));
		assertFalse(afndE.procesar("010"));
		assertFalse(afndE.procesar("110"));
	}

	/**
	 * Este test busca probar el funcionamiento de un automata finito que verifica
	 * que el string comience con 00, la diferencia los anteriores es que los
	 * estados q0 y q1 posee dos transciones con el simbolo 0
	 */
	@Test
	void testAfndWithMultipleTf() {

		State q0 = new State("q0", false);
		State q1 = new State("q1", false);
		State q2 = new State("q2", true);

		q0.addTransitionFunction(new TransitionFunction("0", q0));
		q0.addTransitionFunction(new TransitionFunction("0", q1));
		q1.addTransitionFunction(new TransitionFunction("0", q2));
		q1.addTransitionFunction(new TransitionFunction("0", q0));
		q1.addTransitionFunction(new TransitionFunction("0", q1));
		q2.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("1", q2));

		Afnd afndE = new Afnd(Arrays.asList("0", "1"), Arrays.asList(q0, q1, q2));

		assertTrue(afndE.procesar("000"));
		assertTrue(afndE.procesar("001"));
		assertTrue(afndE.procesar("000101010101"));
		assertTrue(afndE.procesar("001010101001"));
		assertFalse(afndE.procesar("100"));
		assertFalse(afndE.procesar("010"));
		assertFalse(afndE.procesar("110"));
	}

	/**
	 * Este test busca probar el funcionamiento de un automata finito con
	 * transciones epsilon que verifica que el string comience con 0
	 */
	@Test
	void testSimpleAfnde() {

		State q0 = new State("q0", false);
		State q1 = new State("q1", false);
		State q2 = new State("q2", true);

		q0.addTransitionFunction(new TransitionFunction("0", q1));
		q1.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("1", q2));
		q1.addClosure(q2);

		Afnd afndE = new Afnd(Arrays.asList("0", "1"), Arrays.asList(q0, q1, q2));

		assertTrue(afndE.procesar("0"));
		assertTrue(afndE.procesar("01"));
		assertTrue(afndE.procesar("00"));
		assertTrue(afndE.procesar("011"));
		assertTrue(afndE.procesar("001"));
		assertFalse(afndE.procesar("101"));
		assertFalse(afndE.procesar("1"));
		assertFalse(afndE.procesar(""));
	}

	/**
	 * Este test busca probar el procesamiento de un simbolo que no se encuentre
	 * definido
	 */
	@Test
	void testAfndException() {

		State q0 = new State("q0", false);
		State q1 = new State("q1", false);
		State q2 = new State("q2", true);

		q0.addTransitionFunction(new TransitionFunction("0", q1));
		q1.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("0", q2));
		q2.addTransitionFunction(new TransitionFunction("1", q2));

		Afnd afndE = new Afnd(Arrays.asList("0", "1"), Arrays.asList(q0, q1, q2));

		assertThrows(InvalidSymbolException.class, () -> afndE.procesar("0002"));

	}

}
