package exercise1.automatons;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exercise1.exceptions.InvalidSymbolException;
import exercise1.models.State;
import exercise1.models.TransitionFunction;

public class TestSimpleAfd {

	private Afd afd;

	@BeforeEach
	void setUp() {
		State a = new State("A", true);
		State b = new State("B", true);
		State c = new State("C", false);

		a.addTransitionFunction(new TransitionFunction("0", a));
		a.addTransitionFunction(new TransitionFunction("1", b));

		b.addTransitionFunction(new TransitionFunction("0", a));
		b.addTransitionFunction(new TransitionFunction("1", c));

		c.addTransitionFunction(new TransitionFunction("1", c));

		this.afd = new Afd(Arrays.asList("0", "1"), Arrays.asList(a, b, c));
	}

	@Test
	void testEmpty() {
		assertTrue(afd.procesar(""));
	}

	@Test
	void testEndInC() {
		assertFalse(afd.procesar("11"));
	}

	@Test
	void testStateNotDefined() {
		assertFalse(afd.procesar("110"));
	}

	@Test
	void testEndInA() {
		assertTrue(afd.procesar("000"));
	}

	@Test
	void testEndInB() {
		assertTrue(afd.procesar("101010101"));
	}

	@Test
	void testInvalideSymbol() {
		assertThrows(InvalidSymbolException.class, () -> afd.procesar("101010102"));
	}
}
