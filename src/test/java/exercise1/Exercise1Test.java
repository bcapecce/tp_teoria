package exercise1;

import org.junit.jupiter.api.Test;

public class Exercise1Test {

	@Test
	void testCallMainWithoutFile() {
		Exercise1.main(new String[] {});

	}

	@Test
	void testCallMain() {

		ClassLoader classLoader = getClass().getClassLoader();
		String resourceFileName = classLoader.getResource("rules.txt").getFile();

		Exercise1.main(new String[] { resourceFileName });

	}

}
