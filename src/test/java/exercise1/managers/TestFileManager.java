package exercise1.managers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class TestFileManager {
	/**
	 * Este test busca probar que no se encuentra el archivo
	 */
	@Test
	void testLoadInvalidFileName() {
		FileManager fl = new FileManager();
		assertFalse(fl.readFile("invalidName.txt"));

	}

	/**
	 * Este test busca probar la carga del archivo de ejemplo del tp
	 */
	@Test
	void testLoadRulesFile() {

		ClassLoader classLoader = getClass().getClassLoader();
		String resourceFileName = classLoader.getResource("rules.txt").getFile();

		FileManager fl = new FileManager();
		fl.readFile(resourceFileName);
		assertEquals(Arrays.asList("a", "b", "c", "d"), fl.getAlphabet());
		assertEquals(10, fl.getStatesQuantity());
		assertEquals(new HashSet<>(Arrays.asList("1", "2", "3")), fl.getFinalStates());
		assertEquals(5, fl.getTransitionFunctions().size());
	}
}
