# Trabajo Práctico de Teoría de la Computación

## Equipo

- Evelin Aragon
- José Luis Mieres
- Oscar Umbert
- Bruno Capecce

## Name
TP Teoría de la computación.

## Enunciando

### Consideraciones generales
Implementar los siguientes ejercicios en lenguaje Java. La entrega debe realizarse el día martes 23 de
noviembre. Puede no constar de todos los ejercicios, pero debe contener al menos ejercicios por un total
de 5 puntos. Cada ejercicio debe incluir tests unitarios con los cuales mostrar la funcionalidad. La entrega
consta de una demo remota durante la clase de ese día, y el envío por mail del código hasta las 18hs
de ese día. No se recibirán TPs luego de ese momento. El trabajo práctico puede realizarse en grupos
conformados por 3 o 4 personas. No es necesario entregar informe, ni decisiones tomadas, ni conclusiones,
ni verso. Un diseño adecuado del código es parte de la evaluación.

### Ejercicio 1
Sea la siguiente definición de un ε-AFND, que debe leerse de un archivo:

```
<símboloInput>, <símboloInput>, ..., <símboloInput>
<cantEstados>
<estadoFinal>, <estadoFinal>, ..., <estadoFinal>
<estado>, <símboloInput> -> <estado>
<estado>, <símboloInput> -> <estado>
```
La primera línea lista los elementos del alfabeto de input. La segunda indica la cantidad de estados,
la tercera el conjunto de estados finales, y el resto de las líneas la funci´on de transici´on (los espacios
extra en el archivo deben ignorarse). El caracter E lo reservamos para una transici´on ε. Ejemplo:

```
a, b, c, d
10
1, 2, 3
1, a -> 3
1, a -> 4
1, a -> 5
1, d -> 4
4, E -> 4
```

Asumimos que el estado 1 siempre será el inicial, y que los estados están numerados correlativamente de 1 a la cantidad de estados. 
Se pide:

a) Transformar el ε-AFND en AFD.

b) Dar un método procesar(String w) del AFD, que tome un string compuesto de elementos
del alfabeto, y devuelva verdadero o falso seg´un si el string pertenece o no al lenguaje.

### Ejercicio 2
Dada una gramática G libre de contexto leída desde un archivo, implementar un parser
LL(0) para la gramática G utilizando el algoritmo visto en clase.
El formato del archivo debe ser un conjunto de líneas:

```
<variable> -> <body>
```

que representan las producciones de G, donde <variable> es un string de la forma X_{i} con i
un número entero. y <body> es un string conformado por variables como las indicamos recién y/o
caracteres en minúscula, con los que denotamos los terminales de la gramática. Por ejemplo, esta
sería una línea válida:

```
X_{4} -> X_{34}aX_{1}bcd
```
Asumimos que X_{1} es el símbolo inicial.

### Ejercicio 3
Programar un simulador de máquinas de Turing. El simulador debe tomar la definición

```
< Q, Σ, Γ, δ, q0, B, F >
```

de una máquina de Turing (en el formato que les resulte más conveniente) y un string de input ω y
correr el programa de la máquina. Listar por consola cada uno de los movimientos de la máquina de
Turing. Diseñar una regla heurística para sospechar que se colgó y detener la ejecución del simulador
en ese caso.



## Resolución

La resolución se encuentra en las siguientes clases con sus respectivos tests

- exercise1.Exercise1
- exercise2.Exercise2
- exercise3.Exercise3


### Requisitos

- Java 11
